import React, { Component } from 'react';

//import logo from './logo.svg';
import './App.css';


class App extends Component {
  
  static urlbase = 'http://localhost:3007';

  constructor() {
		super();
		this.state = {todos: [], selected:[], id:'', topic:''};
    //{"id":"1","topic":"todo1"},{"id":"4","topic":"todo 4"}

	}

	componentDidMount() {

		fetch(App.urlbase + `/todolist`) 
            .then(result=> result.json())
            .then(todoarr=> {
                //console.log(todoarr);
                this.setState({todos:todoarr});
              })

	}


  handleFormSubmit = formSubmitEvent => {
    formSubmitEvent.preventDefault();
  };


  handleSubmit(event) {

    event.preventDefault();
    console.log(event.target.name);
    if(event.target.name==='add') {
      this._add(this.state.id, this.state.topic);
    }
    //if any deletion
    if(event.target.name==='del') {
      this.state.selected.forEach(function(element) {
        console.log(element);
        this._del(element);
      }, this);
    }
    

  }


  _add(pid, ptopic) {

      fetch(App.urlbase +'/todoadd', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'id='+ pid+ '&topic='+ ptopic
      })
      .then(resultadd => {
        console.log(resultadd);

        if (resultadd.ok){

          fetch(App.urlbase + `/todolist`) 
              .then(result=> result.json())
              .then(todoarr=> {
                  //console.log(todoarr);
                  this.setState({todos:todoarr});
                  this.setState({topic: ''});
                })

          }
      })

  }


  _del(pid) {

      fetch(App.urlbase + '/tododel', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'id=' + pid
      })
      .then(resultdel => {
        console.log(resultdel);

        if (resultdel.ok){

          fetch(App.urlbase + `/todolist`) 
              .then(result=> result.json())
              .then(todoarr=> {
                  //console.log(todoarr);
                  this.setState({todos:todoarr});
                })

          }
      })
  }

  _onChange (e) {
    //console.log(e.target.id);
    //console.log(e.target.checked);
    if(e.target.checked)     
      this.state.selected.push(e.target.id)
    else {
      //console.log(this.state.selected.indexOf(e.target.id));
      this.state.selected.splice(this.state.selected.indexOf(e.target.id), 1);
    }

  }


  _onIdChange(e) {
    //console.log(e.target.value);
    this.state.id = e.target.value;
  }

  
  _onTopicChange(e) {
    //console.log(e.target.value);
    this.setState({topic: e.target.value});
    //this.state.topic = e.target.value;
  }


	render() {
		return(
      <div>
        <div>
          <div>Todos:</div>
            { 
                this.state.todos.map(item=> {
                  //console.log(todo);  
                  return <div>
                    <input type="checkbox" id={item.id} onChange={this._onChange.bind(this)}/>
                    <span>{item.topic}</span></div>
                }) 
            }          
        </div> 
        <div><p></p></div>
        <div className="form-container">
          <form ref='user_form' onSubmit={this.handleFormSubmit} >
            
            <div >
              <label >Topic</label>
              <input name="topic" type="text" value={this.state.topic} id="topic" placeholder="Topic"  onChange={this._onTopicChange.bind(this)}/>
              
            </div>
          
            <button type="submit" className="btn btn-default" name="add" onClick={this.handleSubmit.bind(this)}>
              Adauga
            </button>
            <button type="submit" className="btn btn-default" name="del" onClick={this.handleSubmit.bind(this)}>
              Sterge
            </button>
          </form>
        </div>
      </div>
             
        );
	}



  /*
<div>
              <label >ID </label>
              <input name="id" ref="id" type="text" id="id" placeholder="ID" onChange={this._onIdChange.bind(this)}/>
            </div>
  */
}



export default App;
