var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var fs = require("fs");



// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


var port = 3007; 
var path = "./todo.txt";


app.get('/', function(req, res) {
    res.send('Hello!');
});




app.post('/todoadd', function(req, res) {

	res.header("Access-Control-Allow-Origin", '*');
	res.header("Access-Control-Request-Methods", 'POST');

  	console.log('add');

	fs.readFile(path, function read(err, data) {

     if (err) {
       console.error("write error:  " + error.message);
       res.send('err');

     } else {

       var dataRet = JSON.parse(data.toString());

		  //console.log(data);
		  var nextid = dataRet.todos.length>0?parseInt(dataRet.todos[dataRet.todos.length-1].id) + 1:1;
		  console.log('nextid ' + nextid);


		  dataRet.todos.push({"id": nextid, "topic": req.body.topic});
		  

		  fs.writeFile(path, JSON.stringify(dataRet), function(error) {
		  	console.log('written:' + JSON.stringify(dataRet));
		     if (error) {
		       console.error("write error:  " + error.message);
		       res.send('err');
		     } else {
		       console.log("Successful Write to " + path);
		       res.send('ok');
		     }
			});

     }
	});
  
});



app.get('/todolist', function(req, res) {

	res.header("Access-Control-Allow-Origin", '*');
		res.header("Access-Control-Request-Methods", 'POST');
  console.log('list');
  fs.readFile(path, function read(err, data) {

     if (err) {
       console.error("write error:  " + error.message);
       res.send('err');

     } else {
     console.log(data.toString());

       var dataRet = JSON.parse(data.toString());
       console.log(dataRet);
       res.send(JSON.stringify(dataRet.todos));

     }
   });
  
});




app.post('/tododel', function(req, res) {

	res.header("Access-Control-Allow-Origin", '*');
	res.header("Access-Control-Request-Methods", 'POST');

 	console.log('del');

	fs.readFile(path, function read(err, data) {

     if (err) {
       console.error("write error:  " + error.message);
       res.send('err');

     } else {

	       var dataRet = JSON.parse(data.toString());

	       dataRet.todos.forEach((item, index, object) => {
				if(req.body.id == item.id){
					object.splice(index,1);
					console.log('index deleted:' + index);
				}
			})


		  //console.log(data);
		  //dataRet.todos.push({"id": req.body.id, "topic": req.body.topic});
		  console.log(dataRet);

		  fs.writeFile(path, JSON.stringify(dataRet), function(error) {

		     if (error) {
		       console.error("write error:  " + error.message);
		       res.send('err');
		     } else {
		       console.log("Successful Write to " + path);
		       res.send('ok');
		     }
			});

		  //res.send('ok');

     }
	});
  
});





app.listen(port);

console.log('Run node rest http://localhost:' + port);


